# Azure Functions

[Return to main folder](../README.md)

## Azure Setup
Before you can create functions there are a few things you need to setup first.\
When doing this setup you can download templates and do the setup via Azure cli or Powershell the next time

1. Create an Azure account
    * Head over to the [Azure portal](https://portal.azure.com/) and sign up. On intial signup you will get some free credit to play around with 😀

2. Create a resource group
    A resource group is a container that holds related resources for an Azure solution. The resource group can include all the resources for the solution, or only those resources that you want to manage as a group.

    * Navigate to resource groups and click on Add\
    ![image](images/add-resource-group.png)

    * Select your Azure subscription, give the group a name and select a region for it (the region here doesn't really matter)\
    ![image](images/resource-group-basics.png)

    * On the bottom of the page click on review and create.\
    You can also add tags to the resource group to organize your Azure resources and management hierarchy if you want to. Check the [docs](https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/tag-resources)\
    ![image](images/review-resource-group.png)

    * If validation has passed then click on create\
    ![image](images/review-resource-group.png)


3. Create a storage account
    We need to create a storage account because all the code for your functions will reside in there. We will also use the blob storage and queue storage when injesting data.

    * Navigate to resource groups and select the ProgrammableBanking group we made in the second step\
    ![image](images/pb-resource-group.png)

    * Click on Add to create a new resource\
    ![image](images/pb-add-resource.png)

    * Search for `Storage account`\
    ![image](images/new-storage-account.png)

    * Click on Create\
    ![image](images/create-storage-account.png)

    * On this page there are a few things to fill in and select.\
    Most important is to select the right resource group, make sure your storage account name is uniqe across all of Azure (try a few options till you find something that is free) and to make sure the account type is V2 storage.\
    I've selected locally redundant storage, you should read up on the options and note that they may affect where the data is saved and the cost.\
    ![image](images/pb-storage-account.png)

    * In the network options you can select how the storage account can be accessed. I haven't invested the time into researching all the options here. Public endpoint is the default option which is probably not ideal but for now thats the option I'm going with\
    ![image](images/pb-storage-account-network.png)

    * Also take some time to review the options for Data protection and the Advanced settings.

    * On the bottom of the page click on `Review + create` then on the following page click `Create`\
    ![image](images/review-resource-group.png)

    * In the ProgrammableBanking resource group you will now see the newly created storage acocunt. Take some time to explore the content in the portal when you navigate to the storage account\
    ![image](images/pb-storage-account-done.png)


## Development Environment Setup
I'm using [vscode](https://code.visualstudio.com/) in the examples because it has nice tooling for dev, debugging and deploying to Azure, however you can use whatever you are comfortable with.

This section will mostly cover setting up vscode and the Azure tools

1. Install all the extensions you need

    * Install the Azure Account, Azure Functions and Azure Storage extensions in vscode from here https://code.visualstudio.com/docs/azure/extensions

    ![image](images/vscode-extensions.png)
    * Use the Azure Account extension to sign in to your account from vscode. 
    * When you click on the Azure icon in vscode you should see the Functions extension and Storage extension have been connected. You should be able to browse your storage account from here\
    ![image](images/vscode-azure-extension.png)
    * You can also create new storage accounts from here

2. Install the pre-requisites from the following link depending on what programming language you want to use\
    https://docs.microsoft.com/en-us/azure/azure-functions/functions-create-first-function-vs-code


## Create a Function

### Function setup in Azure

First we need to create a Function app in Azure

1. In the portal click on `Create a resource` or in the resource group page click on `Add` and search for Function App\
    ![image](images/func-create-new.png)
2. Fill in the required details and note that the function needs to have a unique name (same as the storage account)\
    In my examples I'm using C# and dotnet core. The reason for this is I want to use Consumption hosting and SA doesn't have that many options for this yet.\
    Then click on `Hosting`\
    ![image](images/func-create-name.png)
3. In the Hosting options select the storage account you created and leave the operating system as Windows.\
    For the plan type select `Consumption (Serverless)`. On this plan you will only be charged for the resources you use to run the code.\
    You will see the Linux option only has `App Service Plan` at this point which charges you for dedicated resources to run your function (read more expensive)\
    ![image](images/func-create-hosting.png)
4. Go to the monitoring page and turn off application insights if you want to. I don't know if there are extra charges for this
    ![image](images/func-create-monitor.png)
5. Click on `Review + create` and then on `Create`

### Create Function code

Finally, some code!

1. Open vscode and click on Create new project in the Functions extension\
    ![image](images/vscode-create-function.png)
2. Select a folder for your project\
    ![image](images/vscode-create-function-browse.png)
3. Select the programming language (I'm going to use C# for the example becuse Consumption hosting isn't availble in SA yet but will also include a C# example)\
    ![image](images/vscode-create-function-language-1.png)
4. For different languages there might be some specific prompts that follow.
5. Select the trigger for your function. We will use HTTP trigger since we will post to the endpoint from the programmable banking platform.\
    ![image](images/vscode-create-function-trigger.png)
6. Give the function a name and enter a namespace\
    ![image](images/vscode-create-function-name.png)\
    ![image](images/vscode-create-function-name-space.png)
7. Select the Access level. We will use Function to enable us to use a function API key to limit access to the endpoint. I suggesst you don't use Anonymous since then anyone can access the resource over the internet\
    ![image](images/vscode-create-function-auth.png)
8. You should now see your brand new function\
    ![image](images/vscode-create-function-done.png)

### Publish Function

Now I'm immediately going to publish my function before changing any logic. The reason for this is I want to get the storage binding included so I can persist data to the storage account

1. In the Functions extension click on `Deploy to Function App...`\
    ![image](images/vscode-deploy.png)
2. Select the fuction you want to deploy to\
    ![image](images/vscode-deploy-select.png)
3. Confirm\
    ![image](images/vscode-deploy-confirm.png)
4. In the output you should see the app being published and deployed to Azure\
    ![image](images/vscode-deploy-publish.png)
5. When done you will see the following message\
    ![image](images/vscode-deploy-done.png)

### Verify Function

In the portal navigate to the function and click on `Functions` in the menu.\
Click on the Function so we can get the URL for it.\
![image](images/func-overview.png)

On the next page click on `Get Function URL`\
![image](images/func-overview-url.png)

Copy the default function key.\
You will see in the URL there is a key passed as a paramter (`?code=/`). This is the function key that allows access to call our function. **Don't share this with anyone.**\
Paste the URL into your browser and verify it is working\
![image](images/func-overview-verify.png)


### Setup storage binding

Next we will setup a storage binding so we can write to the storage account. This is basically the same tutorial from the [docs](https://docs.microsoft.com/en-us/azure/azure-functions/functions-add-output-binding-storage-queue-vs-code?pivots=programming-language-csharp)

1. Execute the command (Ctrl+Shift+P or Cmd+Shift+P) `Azure Functions: Download Remote Settings...`\
![image](images/setup-storage-command.png)
2. Select the Function\
![image](images/setup-storage-func.png)
3. Click Yes to overwrite local settings\
![image](images/setup-storage-confirm.png)

This will add a `"AzureWebJobsStorage"` key to your local settings which will allow the Function to connect to the storage account when running locally.
** Once again, do not share this key with anyone**


### Write to message queue

1. Register the binding extension by running the following command in the terminal
```
dotnet add package Microsoft.Azure.WebJobs.Extensions.Storage --version 3.0.4
```
2. Replace the code with the following\
    Note that we are removing the `get` option for our endpoint and only accepting `post`.\
    We will be writing to a queue called `transactions` and will add the json object into the message
```C#
    [FunctionName("HttpExample")]
    public static async Task<IActionResult> Run(
        [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req,
        [Queue("transactions"),StorageAccount("AzureWebJobsStorage")] ICollector<string> queue,
        ILogger log)
    {
        log.LogInformation("C# HTTP trigger function processed a request.");

        string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

        if (!string.IsNullOrEmpty(requestBody))
        {
            queue.Add(requestBody);
            return (ActionResult)new OkObjectResult("Success");
        }
        else
        {
            return new BadRequestObjectResult("Failed to write to message queue");
        }
    }
```

3. Run the app locally and call the endpoint.\
![image](images/storage-write-queue.png)

4. Verify the messages was placed in the queue.\
    View the storage account on the Azure portal\
    ![image](images/storage-write-queue-1.png)\
    Go to the storage explorer and verify they queue was created and the message was placed on the queue\
    ![image](images/storage-write-queue-2.png)

5. Deploy these changes to Azure so we can call it from the Programmable Banking Platform
6. Call the Function in Azure and verify everything works


### Send data from the Programmable Banking Platform

1. Add the following code to your programmable card's `main.js` file\
    Note: This is a shameless copy of the code from [Jeremy's project](https://github.com/JeremyWalters/investec-logs-functions)
```JavaScript
    // This function runs before a transaction.
    const beforeTransaction = async (authorization) => {
        console.log(authorization);
        return true;
    };

    // This function runs after a transaction was successful.
    const afterTransaction = async (transaction) => {
        // Log transaction
        console.log(transaction);
        await postTransaction(transaction);
    };

    // Post transaction to Azure
    async function postTransaction(transaction) {
        try {
            let response = await fetch(process.env.functionEndpoint, {
                method: 'POST',
                headers: { 
                    'Content-Type': 'application/json', 
                    'x-functions-key': process.env.functionApiKey
                },
                body: JSON.stringify(transaction)
            });

            const result = await response.text();
            console.log(result);
        } catch(error) {
            console.log(error)
        }
    }
```

2. Place replace all needed variables in the `env.json` file:
```JavaScript
    {
        "functionEndpoint": "https://<function-app>.azurewebsites.net/api/<function-name>",
        "functionApiKey": "<function api key> - Key can be found in the functions URL on the portal"
    }
```

3. Simulate a transaction and verify that it works\
    ![image](images/simulation-1.png)\
    ![image](images/simulation-2.png)


### Write to table storage / CosmosDB

1. TODO